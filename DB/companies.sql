-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 08, 2017 at 02:41 PM
-- Server version: 5.7.20-log
-- PHP Version: 7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `account_number`, `address`, `created_at`, `updated_at`) VALUES
(2, 'Hogy is hívják Bt.', '347563478563434-5345', '3476 Fudfdu 3.', NULL, NULL),
(3, 'Trabant Zrt', '3453453345-122342342', '4839 Dgheoh e3', NULL, NULL),
(4, 'Ez egy új Kft.', '3454353453453454545', '2354 eperfa 34', NULL, NULL),
(5, 'National Instruments', '34534534534-345345345', '4023 Debrecen Fő u5', '2017-12-08 09:59:51', '2017-12-08 09:59:51'),
(10, 'IBM', '553466546-546346346', '43534253425', '2017-12-08 11:28:43', '2017-12-08 11:28:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(57, '2014_10_12_000000_create_users_table', 1),
(58, '2014_10_12_100000_create_password_resets_table', 1),
(59, '2017_12_07_122157_create_company_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `companies_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `companies_id`) VALUES
(1, 'Hóka Móka', 'hokamoka@gmail.com', '$2y$10$A067M3eefMRPjGr8.ZQdOuSqjqqJ2PzyTQMgLZUdJ1Xhl.cicZ39m', 'VwP45nz7N1bw7zJcAOEvuvLIl1ZxuMEKUjZlAxmpQpmsClhNUk04FJNqfj9e', NULL, '2017-12-08 08:23:37', 3),
(2, 'Gellei Csaba', 'gellei.csaba@gmail.com', '$2y$10$G8JXkwlymRq7CJfHOYbChugfIMwVOcwQBLQdXlXqZAQd0xejwcnxe', 'GefsyBODCT3aUAIVCjPGYSHW6vWFbzu3CcbAgbOjhEEvpXglbZ4oj8Tg1MC9', NULL, '2017-12-08 08:23:33', 4),
(3, 'Kajak Lajos', 'kdsfj@dfff.net', '$2y$10$qRIKg86cq3v6MlERkvSq2uAUEH86BLcmI4mFm0PlN6QqApNl94VTe', 'WJZmbAueZxR6DVUNQyT3IW60096Eyo735s4EDe3qFqeiV1OSDRL5ZLqHLErv', '2017-12-08 09:33:49', '2017-12-08 09:33:59', 3),
(4, 'Maci Laci', 'mgmmd@gmail.gu', '$2y$10$AwPSmvO.74T1.iZnGYiLVOK0BkP1PdutNhT3LacZklKTwN4veid.K', 'eJmccJOzaVgcsM28hGSEwGLwA3ev0jO0mCtvTuuuyRmmLc8W5ghcWDK3HeRc', '2017-12-08 09:35:18', '2017-12-08 09:35:43', 2),
(5, 'Kozma Ferenc', 'kokov@gm.dd', '$2y$10$qwmHhLWsfRRE7BcPubZZdeEdQtYTKFWFkaqRDNgPI85HmkeVxZbZi', '5vfyoeHSqmFdLiJs60xoqhgt9R309yOn6bRxcF3qQeEuZ7RtJweMTrzPk7zA', '2017-12-08 10:00:51', '2017-12-08 10:04:17', 3),
(6, 'John Doe', 'go@nk.net', '$2y$10$F8Fwxz63VBbXsD2XnUWVJONBMV7dkMoZVLNjPwc3iDaxQlVO4X3qC', '9eqV1El2RXPxhVpsXGtKg5KELQIeNPW8od5yiypEkh4MSQuaEbV24w9RP6RT', '2017-12-08 10:03:42', '2017-12-08 10:42:04', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_companies_id_index` (`companies_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
