<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    protected $table = "companies";

    public function users()
    {
        return $this->hasMany('App\User', "companies_id", "id" );
    }
}
