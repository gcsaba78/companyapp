<?php

namespace App\Http\Controllers;

use App\Companies;
use App\User;
use View;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class CompaniesController extends Controller
{

    public function index()
    {
        $companies = Companies::all()->sortBy("name");
        return View::make('pages.companies.index')->with('companies', $companies);
    }

    public function create()
    {
        return View::make('pages.companies.create');
    }

    public function store()
    {
        $validator = $this->getValidator();

        if ($validator->fails())
        {
            return Redirect::to('companies/create')->withErrors($validator)->withInput();
        }
        else
        {
            // check duplicate
            $companies = Companies::Where("name", Input::get('name'));
            if ($companies != null || sizeof($companies) > 0)
            {
                return Redirect::to('companies/create')->withErrors("Már van ilyen company!");
            }
            // store
            $companies = new Companies();
            $companies->name = Input::get('name');
            $companies->account_number = Input::get('account_number');
            $companies->address = Input::get('address');
            $companies->save();

            // redirect
            Session::flash('message', 'Successfully save!');
            return Redirect::to('companies');
        }

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $companies = Companies::find($id);
        // show the edit form and pass the nerd
        return View::make('pages.companies.edit')->with('companies', $companies);
    }

    public function update($id)
    {
        $validator = $this->getValidator();

        if ($validator->fails())
        {
            return Redirect::to('companies/' . $id . '/edit')->withErrors($validator)->withInput();
        }
        else
        {
            // store
            $company = Companies::find($id);
            $company->name = Input::get('name');
            $company->account_number = Input::get('account_number');
            $company->address = Input::get('address');
            $company->save();

            // redirect
            Session::flash('message', 'Successfully save!');
            return Redirect::to('companies');
        }
    }

    private static $rules = array(
        'name' => 'required|min:3|max:100',
        'account_number' => 'required|min:16|max:26',
        'address' => 'required|max:150',
    );

    private static $messages = [
        'required' => 'Az ":attribute" mező kötelező.',
        'min' => 'Az ":attribute" mező hossza nem lehet kisebb :min karakternél.',
        'max' => 'Az ":attribute" mező hossza nem lehet nagyobb :max karakternél.',
    ];

    private function getValidator()
    {
        return Validator::make(Input::all(), CompaniesController::$rules, CompaniesController::$messages);
    }

    public function destroy($id)
    {
        // delete

        $companies = Companies::find($id);

        /*
        $companies->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the nerd!');
        return Redirect::to('nerds');
        */

        return View::make('pages.companies.delete')->with('companies', $companies);
    }

    public function delete($id)
    {
        $company = Companies::find($id);

        //delete related users
        $users = $company->users;
        foreach ($users as $user)
        {
            $user->companies_id = null;
            $user->save();
        }

        $company->delete();
        // redirect
        Session::flash('message', 'Successfully deleted!');
        return Redirect::to('companies');
    }


}
