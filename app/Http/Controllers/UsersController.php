<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Companies;
use View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class UsersController extends Controller
{
    public function index()
    {
        $users = User::all()->sortBy("name");
        return View::make('pages.users.index')->with('users', $users);
    }

    public function edit($id)
    {
        // user data
        $user = User::find($id);
        $data["user"] = $user;

        // company dropdown list
        $companies = Companies::all();
        $list = [];
        foreach($companies as $company)
            $list[$company->id] = $company->name;
        $data["companylist"] = $list;

        return View::make('pages.users.edit')->with('data', $data);
    }

    public function update($id)
    {
        $xy = Input::get('company_id');
        echo "updated to ", $xy;

        $user = User::find($id);
        $user->companies_id = intval(Input::get('company_id'));
        $user->save();

        // redirect
        Session::flash('message', 'Successfully save!');
        return Redirect::to('users');

    }

}
