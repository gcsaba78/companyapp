<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Companies;
use View;


class ListController extends Controller
{
    public function index()
    {
        $companies = Companies::all()->sortBy("name");
        return View::make('pages.companies.list')->with('companies', $companies);
    }
}
