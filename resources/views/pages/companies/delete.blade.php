@extends('layouts.app')
@section('content')
    <div style="width: 800px; margin: 40px 40px 40px 40px;">

        <div class="form-group">
            Name:
            {{ $companies->name }}
        </div>

        <div class="form-group">
            Address:
            {{ $companies->address }}
        </div>


        <div class="form-group">
            Are you sure to delete this company?
        </div>

        <a href="/companies/{{ $companies->id }}/delete" class="btn btn-primary">Yes</a>
        <a href="/companies" class="btn btn-primary">No</a>

        {{ Form::close() }}

    </div>
@endsection
