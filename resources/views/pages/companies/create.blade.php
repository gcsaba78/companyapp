@extends('layouts.app')
@section('content')
    <div style="width: 800px; margin: 40px 40px 40px 40px;">
        {{ Form::open(['url' => 'companies']) }}

        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('account_number', 'Account number') }}
            {{ Form::text('account_number', null, array('class' => 'form-control')) }}
        </div>

        <div class="form-group">
            {{ Form::label('address', 'Address') }}
            {{ Form::text('address', null, array('class' => 'form-control')) }}
        </div>

        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        <a href="/companies" class="btn btn-primary">Cancel</a>

        {{ Form::close() }}
    </div>
    <span style="color:red; font-weight: bold">{{ Html::ul($errors->all()) }}</span>
@endsection
