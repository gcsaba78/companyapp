@extends('layouts.app')
@section('content')

    <h1>Company list</h1>
    <style>
        .company_item{ border-width: 1px 1px 1px 1px; border-color: black; }
        .user_item{ border-width: 1px 1px 1px 1px; border-color: black; }
    </style>

    <ul>

    <?php $c = 0; ?>
    @foreach($companies as $company)
    <li>
        <div class="company_item">
            {{ ++$c }}.
            <b>Company :</b> {{ $company->name }},&nbsp;{{ $company->address }}
        </div>

        <?php $d = 0; ?>
        @if (isset($company->users))
            <ul>
                @foreach($company->users as $user)
                <li>
                    <div class="user_item">
                        {{ ++$d }}. {{ $user->name }},&nbsp;{{ $user->email }}
                    </div>
                </li>
                @endforeach
            </ul>
        @endif

    </li>
    @endforeach

    </ul>
@endsection