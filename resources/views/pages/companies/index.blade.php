@extends('layouts.app')
@section('content')
    <div class="panel panel-default" style="width: 1200px; margin:50px 50px 50px 50px;">
        <div class="panel-heading">Companies</div>
        <div class="panel-body">

            <a class="btn btn-small btn-info" href="{{ URL::to('companies/create') }}">Create a company</a><br>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Account number</td>
                    <td>Address</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>

                @foreach($companies as $key => $value)
                    <tr>
                        <td>{{ $value->name }}</td>
                        <td>{{ $value->account_number }}</td>
                        <td>{{ $value->address }}</td>
                        <td>
                            <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                            <a class="btn btn-small btn-info pull-left" href="{{ URL::to('companies/' . $value->id . '/edit') }}">Edit this company</a>
                            {{ Form::open(array('url' => 'companies/' . $value->id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Delete this company', array('class' => 'btn btn-small btn-warning')) }}
                            {{ Form::close() }}

                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>
    </div>
@endsection
