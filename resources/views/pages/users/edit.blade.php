@extends('layouts.app')
@section('content')

    <div class="form-group">
        Name: {{ $data["user"]->name }}
    </div>
    <div class="form-group">
        Email: {{ $data["user"]->email }}
    </div>

    {{ Form::model($data["user"], array('route' => array('users.update', $data["user"]->id), 'method' => 'PUT')) }}


    <div class="form-group">
    {{ Form::select('company_id', $data["companylist"], null) }}

    {{-- set default value --}}
    <script>
        window.onload = function(e) {
            //document.getElementById('company_id').value =
            document.getElementsByName("company_id")[0].value = {{ $data["user"]->companies_id }};
        };
    </script>

    </div>

    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    <a href="/users" class="btn btn-primary">Cancel</a>

    {{ Form::close() }}


@endsection