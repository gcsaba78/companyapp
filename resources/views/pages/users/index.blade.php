@extends('layouts.app')
@section('content')
<div class="panel panel-default" style="width: 1200px; margin:50px 50px 50px 50px;">
    <div class="panel-heading">User assignments</div>
    <div class="panel-body">

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Name</td>
                <td>Email</td>
                <td>Company</td>
                <td></td>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->company != null ? $user->company->name : '' }}</td>
                    <td>
                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                        <a href="/users/{{ $user->id }}/edit" class="btn btn-primary">Assign company</a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>
</div>
@endsection