<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('companies')) {
            Schema::create('companies', function (Blueprint $table) {
                $table->increments('id');
            });
        }
        Schema::table('companies', function (Blueprint $table)
        {
            if (! Schema::hasColumn('companies', 'name'))
                $table->string('name', 100);
            if (! Schema::hasColumn('companies', 'account_number'))
                $table->string('account_number', 30);
            if (! Schema::hasColumn('companies', 'address'))
                $table->string('address', 150);
            if (! Schema::hasColumn('companies', 'created_at'))
                $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
